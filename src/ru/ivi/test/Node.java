package ru.ivi.test;

/**
 * Created by lauraldo on 17.08.17.
 */
public class Node {
    Node link;
    private String desc;

    public Node (String desc) {
        this.desc = desc;
    }

    public Node() {
        this.desc = "def";
    }

    @Override
    public String toString() {
        return desc;
    }
}
