package ru.ivi.test;

/**
 * Created by lauraldo on 17.08.17.
 */
public class NodeListManager {

    public static Node reverseLinkedList(Node in) {
        Node result = null;
        Node cur = in;
        while (cur != null) {
            Node next = cur.link;
            cur.link = result;
            result = cur;
            cur = next;
        }
        in = result;
        return in;
    }

    public static String showLinkedList(Node in) {
        StringBuilder result = new StringBuilder();
        Node cur = in;
        while (cur != null) {
            Node next = cur.link;
            result.append("(");
            result.append(cur.toString());
            result.append(")");
            result.append("->");
            if (next == null) {
                result.append("null");
            }
            cur = next;
        }
        return result.toString();
    }

}
