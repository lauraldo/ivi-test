package ru.ivi.test;

public class Main {

    public static void main(String[] args) {

        Node n1 = new Node("in");
        Node n2 = new Node("2");
        Node n3 = new Node("3");
        n1.link = n2;
        n2.link = n3;
        n3.link = null;
        System.out.println(NodeListManager.showLinkedList(n1));
        System.out.println(NodeListManager.showLinkedList(NodeListManager.reverseLinkedList(n1)));
    }
}
